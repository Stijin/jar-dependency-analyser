# JarDependencyAnalyser
In case you run into a 'dependency hell' and all you got is the jars that are causing it this quick 'n dirty script might help out. 

By providing a directory containing the jars that are causing issues (e.g. `/path/to/tomcat/webapps/your-app/WEB-INF/lib`) it tries to build a dependency graph and shows the most likely suspects to cause the dependency hell.

## Installing dependencies
Use the `requirements.txt` to install the required dependencies globally:
```bash
pip3 install -r requirements.txt
python3 jar-dependency-analyser.py --help
```

Or if you want to run it in a virtual environment:
```bash
python3 -m venv .venv
venv/bin/pip install -r requirements.txt
.venv/bin/python3 jar-dependency-analyser.py --help
```

If you are running with a default python3 installation it is possible to run the script as easy as `.jar-dependency-analyser.py --help`.

## Examples
The script will output a list of `.jar` files that reference the same classes. E.g.:
```bash
files                                                                       # of shared references
------------------------------------------------------------------------  ------------------------
('yuicompressor-2.4.2.jar', 'js.jar')                                                          273
('Human_ResourcesService.jar', 'IntegrationService24.1.jar')                                   143
('jms.jar', 'javax.jms.jar')                                                                    58
('stax-api-1.0-2.jar', 'geronimo-stax-api_1.0_spec-1.0.1.jar')                                  33
('sac-1.3.jar', 'batik-ext-1.6-1.jar')                                                          29
('spring-context-4.3.16.RELEASE.jar', 'spring-support-2.0.8.jar')                               21
('javax.annotation-api-1.2.jar', 'geronimo-annotation_1.1_spec-1.0.jar')                        14
('commons-beanutils.jar', 'commons-collections-3.2.2.jar')                                      10
('axis2-kernel-1.6.1.jar', 'axis2-transport-http-1.6.1.jar')                                     9
('grc_audit_log.jar', 'grac_request_details.jar')                                                9
('grac_user_access.jar', 'grac_request_details.jar')                                             9
('grac_user_access.jar', 'grc_audit_log.jar')                                                    9
('grac_risk_analysis_wout_no_ws.jar', 'grac_request_details.jar')                                9
('grac_risk_analysis_wout_no_ws.jar', 'grc_audit_log.jar')                                       9
('grac_risk_analysis_wout_no_ws.jar', 'grac_user_access.jar')                                    9
```
It will also provide a graph of all the files and their 'connections'. The graph can be used to move around and explore connections between the `.jar` files.
![Example of 'connections between `.jar` files'](readme-examples/example1.png)

Keep in mind not every shared reference is a potential problem, but it will help you find the actual problems more easily.