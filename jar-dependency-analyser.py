#!/usr/local/bin/python3

import argparse
import os
import logging
import sys
import mmap
import re
import collections
import networkx
import matplotlib.pyplot as plt
import itertools
import tabulate
import operator

class JarDependencyAnalyser:

    def __init__(self):
        self.debug_mode = False
        self.classes_found_per_file = {}
        self.duplicate_classes = {}
        self.classes_found = []
        self.setup_logging()
        self.parse_arguments()
        self.validate_arguments()
        self.analyse()
        self.report()

    def setup_logging(self):
        FORMAT = '[%(levelname)s] %(message)s'
        logging.basicConfig(format=FORMAT)
        self.log = logging.getLogger('jar-dependency-analyser')
        self.log.setLevel(20)
    
    def parse_arguments(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('directory', help='Path to the directory containing the jars you want to analyse')
        parser.add_argument('--debug', help='Enable debug output', action="store_true")
        args = parser.parse_args()
        self.directory = args.directory
        if args.debug:
            self.debug_mode = True
            self.log.setLevel(10)
        self.log.debug('Finished parsing arguments, directory being used \'%s\' and debug mode is \'%s\'', self.directory, self.debug_mode)

    def validate_arguments(self):
        if not os.path.exists(self.directory) and not os.path.isdir(self.directory):
            self.log.error('\'%s\' is not a directory!', self.directory)
            self.die()

    def collect_class_references(self, directory):
        for root, directory, files in os.walk(directory):
            for filename in files:
                # only interested in .jar files
                if filename.endswith('.jar'):
                    self.log.debug('Searching class references in \'%s\'', filename)
                    file_path = os.path.join(root, filename)
                    with open(file_path, 'rb', 0) as file, mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as s:
                        pattern = b'([- $ A-Za-z / 0-9 _]*).class'
                        # removing duplicates from classes referenced in one file
                        found_class_references =  list(dict.fromkeys(re.findall(pattern, s)))
                        self.classes_found_per_file[filename] = found_class_references
                        self.classes_found.extend(found_class_references)

    def analyse(self):
        self.log.info('Analysing files in \'%s\'...', self.directory)
        
        # collect class references
        self.collect_class_references(self.directory)

        # check for duplicates
        duplicate_classes = [item for item, count in collections.Counter(self.classes_found).items() if count > 1]

        # collect info on the duplicate classes
        for classname in duplicate_classes:
            self.log.debug('Analysing duplicate class \'%s\'', classname.decode('utf-8'))
            class_found_in_files = [filename for filename in self.classes_found_per_file if classname in self.classes_found_per_file[filename]]
            self.log.debug('Class \'%s\' is found in \'%s\'', classname.decode('utf-8'), class_found_in_files)
            self.duplicate_classes[classname.decode('utf-8')] = class_found_in_files
        
        self.log.info('Found %s files containing class references adding up to a total of %s unique class references. %s of classes were found multiple times', len(self.classes_found_per_file), len(self.classes_found), len(duplicate_classes))

    def report(self):
        # create graph report with 'https://www.python-course.eu/networkx.php'
        # should contain nodes as files containing duplicates class references
        # lines should show on how many times the files are overlapping
        # also drop a file with a full of classnames to further analysis
        self.log.info('Generating report...')
        edges_counts = {}
        graph = networkx.Graph()
        all_files_with_overlap = list(itertools.chain.from_iterable(self.duplicate_classes.values()))
        graph.add_nodes_from(all_files_with_overlap)
        
        for classname in self.duplicate_classes:
            linked_files = self.duplicate_classes[classname]
            for filename in linked_files:
                for filename2 in linked_files:
                    if filename != filename2:
                        # counting the amount of times files are referencing the same classes
                        if edges_counts.get((filename, filename2)) is not None:
                            edges_counts[(filename, filename2)] = edges_counts.get((filename, filename2)) + 1
                        else:
                            # not recording edges from a -> b and b -> a
                            if edges_counts.get((filename2, filename)) is None:
                                edges_counts[(filename, filename2)] = 1
                        graph.add_edge(filename, filename2)
        self.log.debug('Graph contains \'%s\' nodes and \'%s\' edges', len(graph.nodes()), len(graph.edges()))
        
        # print table based on files and the amount of references they share
        sorted_edges = sorted(edges_counts.items(), key=operator.itemgetter(1))
        sorted_edges.reverse()
        print('\n' + tabulate.tabulate(sorted_edges, headers=['files', '# of shared references']) + '\n')

        positions = networkx.spring_layout(graph)
        networkx.draw(graph, pos=positions, edge_color='black', width=1,linewidths=1, \
            node_size=500, node_color='red', alpha=0.9, with_labels=True)
        networkx.draw_networkx_edge_labels(graph, positions, \
            edge_labels=edges_counts,font_color='red')
        plt.show()
        plt.figure()

        self.log.info('Analysis comleted. Hopefully you can find the conflicting jars :).')

    def die(self):
        sys.exit(1)

if __name__ == "__main__":
    jda = JarDependencyAnalyser()
